Text:
All text, apart from that on the buy page, is copied from http://hydromousse.co.nz/.
The information on the buy page is not accurate, and should not be treated as such. I do not know the real locations
that hydro mousse is sold, and therefore had to make stuff up.

Images:
All gallery images and the images in the sidebar, as well as the logo image, are taken from http://hydromousse.co.nz/.
The header image and the main content image behind the text are my own. They were created by me in photoshop.

Video and Audio:
Videos are taken from http://hydromousse.co.nz/, with the audio just being an audio port of one of these videos.


