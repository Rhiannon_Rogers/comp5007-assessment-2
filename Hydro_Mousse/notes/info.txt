Version 1: Added index.html
Version 2: Added body and container css.
Version 3: Added header css.
Version 4: Added navigation css.
Version 5: Added css for content container, including the left, right and middle divisions. Added images.
Version 6: Added footer css.
Version 7: Added desktop layout.
Version 8: Added laptop layout.
Version 9: Added tablet landscape layout.
Version 10: Added tablet portrait layout.
Version 11: Added phone portrait layout.
Version 12: Added phone landscape layout.
Version 13: Added forgotten doctype declaration and did some html and css error fixing. Because I forgot the doctype declaration, my layouts are now very messed up.
Version 14: Removed media queries to focus on bug fixing. I also fixed some bugs to do with the navigation and image displays.
Version 15: Added complete fix to larger desktop layout (base layout).
Version 16: Re-added desktop, laptop and tablet horizontal layouts (all similar) and fixed their bugs.
Version 17: Re-added remaining layouts and fixed their bugs.
Version 18: Created new layout (laptop small) for smaller browser sizes on laptops. This makes things much easier to view.
Version 19: Re-adjusted font-sizes, padding and margins. Fixed bugs in google chrome cause by google chrome browser adding its own margins and paddings.
Version 20: Add buy.html and related css for all layouts.
Version 21: Add gallery.html and related css for all layouts.
Version 22: Add reviews.html and related css for all layouts.
Version 23: Add contactus.html and related css for all layouts.
Version 24: Add faq.html and related css for all layouts.
Version 25: Fixed a bug that was causing different text sizes across pages on my phone layouts. This made my pages inconsistent with each other. I also removed some paddings and margins so that I can later restyle them and make them more consistent across pages.
Version 26: Added padding and margins back to be more consistent across pages.
Version 27: Fixed some bugs cause by browser defaults in certain browsers that I had forgot to cancel out.
Version 28: Added sound files and animations, and an update to the logo image so it can be seen better against the background.
Version 29: Simplified the amount of divs in the header and the footer, so that they are more streamlined, easy to edit and page loading times are reduced.
Version 30: Did some optimising of my css so that is easier to read and less redundant. This should make the page load faster.
Version 31: Removed unneccessary divs on gallery page, to make the code more streamlined and make pages load faster.
Version 32: Combined the left and right divisions into 1 class, as they share a lot of the same code. This reduces css redundancy and makes the code more streamlined.
Version 33: CSS is completed cleaned and optimised. HTML and CSS pages have been validated.
Version 34: Made slight adjustment to header and footer. Gave the header a h1 tag, as its best practice. I also added some explanatory notes and the video files. (These were previously not included as they are a large file size.)